<?php
/**
 * Holds constants for URL Handlers
 * @author Majid Hameed
 * 
 */

$url_handler_constants = parse_ini_file('url-handler-constants.ini', true);
$bad_img = parse_ini_file('bad_imgs.ini', true);

define('MASTER_SERVER_URL', $url_handler_constants['MASTER_SERVER_URL']);
define('URL_PROCESSOR_CONTROLLER', $url_handler_constants['URL_PROCESSOR_CONTROLLER']);
define('BOT_FILTERING_ENABLED',	(bool) $url_handler_constants['BOT_FILTERING_ENABLED']);
define('JS_REDIRECT_ENABLED', (bool) $url_handler_constants['JS_REDIRECT_ENABLED']);
define('PIXEL_IMG', (bool) $url_handler_constants['PIXEL_IMG']);
define('URL_SERVER_CONTROLLER', $url_handler_constants['URL_SERVER_CONTROLLER']);

$URL_PROCESSORS = $url_handler_constants['URL_PROCESSORS'];
$BAD_UA_LIST = $url_handler_constants['BAD_UA_LIST'];

$BAD_IMG_LIST = $bad_img['BAD_IMGS'];

$u_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['u'];
$i_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['i'];
$t_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['t'];
$r_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['r'];
$o_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['o'];
$h_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['h'];

$ga_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['ga'];

$util_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['get'];

$g_processor = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['g'];

$unsubscribe = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['unsubscribe'];
$unsubscribe2 = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['unsubscribe2'];
$doUnsubscribe = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['doUnsubscribe'];
$doUnsubscribe2 = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['doUnsubscribe2'];

$ipBotsUrl = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['IPBots'];

$badImgUrl = MASTER_SERVER_URL . URL_PROCESSOR_CONTROLLER . '/' . $URL_PROCESSORS['BadImgUrl'];

$requestOptions = $url_handler_constants['HTTP_REQUEST_OPTIONS'];

Logger::configure('log4php.xml');
// Fetch a logger, it will inherit settings from the root logger
$logger = Logger::getLogger('url-handlers');
