<?php

/**
 * 
 * Mask Unsubscribe URL Handler - Handles unsubscribe request
 * 
 * @author Majid Hameed
 * @since February 13, 2013 
 *  
 */

require_once 'vendor/autoload.php';
require 'constants.php';
use Guzzle\Http\Client;

ini_set('display_errors', 'Off');

if (empty($_REQUEST['cid'])) { // Get Form Request
	getUnsubscribeForm();
} else {
	processUnsubscribeRequest();
}


function getUnsubscribeForm() {
	global $unsubscribe2;
	
	// Process Request
	if (empty($_REQUEST['id'])) {
		return;
	}
	
	$cid = $_REQUEST['id'];
	
	$requestData = array(
			'cid' => $cid,
			'requestHeaders' => $_SERVER
	);
	
	sendRequest($unsubscribe2, $requestData);
}

function processUnsubscribeRequest() {
	global $doUnsubscribe2;
	
	//$mask = $_REQUEST['h'];
	
	$requestData = array(
			'requestParams' => $_REQUEST,
			'requestHeaders' => $_SERVER
	);
	
	sendRequest($doUnsubscribe2, $requestData);
}


function sendRequest($url, $requestData) {
	$client = new Client($url);
	$request = $client->post('', null, $requestData);
	
	$response = $request->send();
	
	$contentType = $response->getContentType();
	$size = $response->getContentLength();
	
	header("HTTP/1.1 200 OK");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("cache-Control: no-store, no-cache, must-revalidate");
	header("cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header("Content-Type: $contentType");
    if ($size){
        header("Content-Length: $size");
    }
	echo $response->getBody();
}