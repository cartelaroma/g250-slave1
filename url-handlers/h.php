<?php

/**
 * 
 * Mask Image URL Handler - It handles masked Image URL requests from (Campaign) Email URL Links
 * 
 * @author Majid Hameed
 * @since Novemeber 25, 2012
 *  
 */

use Guzzle\Http\Client;
require("CachedImage.php");
$scriptName = basename(__FILE__, '.php');

$logger->debug($scriptName . ' - Valid Request - Request Data - ' . json_encode($requestData));

// POST data to be saved in database
$client = new Client($h_processor);

try {

    $requestData['requestHeaders']['SCRIPT_NAME']  =  "/".$scriptName . '.php';  //Fix script name

    $response = getURL($requestData, $client, true);
    $logger->info($scriptName . ' - response - ' . $response);
	
	if ($response=='INVALID-URL') {
		$response = 'img/eh.gif';
	}

    curl_post_async($h_processor, $_SERVER, $mask);  //To log in the server

    $response = $response['localURL'];

	// FETCH original url
	$originalURL = $response;

    // New part

    $image = new CachedImage($originalURL);

    $image->alterImage();

    header($image->getHeader());

    $image->getImage();

} catch (Exception $e) {
	$logger->error($e->getMessage());
}
