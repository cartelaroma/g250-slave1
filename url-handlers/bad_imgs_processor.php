<?php
/**
 * Created by PhpStorm.
 * User: yancy
 * Date: 21/10/15
 * Time: 5:39
 */
require_once 'vendor/autoload.php';
require 'constants.php';
require 'common.php';
use Guzzle\Http\Client;

ini_set('display_errors', 'On');

$scriptName = basename(__FILE__, '.php');

try{
    get_bad_imgs($badImgUrl);
    //loadIps();
}catch (Exception $e){
    $logger->info($scriptName . ' - Something wrong when processing - ' . json_encode($e));
}

function get_bad_imgs($badImgUrl){
    $client = new Client($badImgUrl);
    $request = $client->post('', null, array(), array('timeout' => 59, 'connect_timeout' => 59));
    $data = $request->send()->json();

    $fileContent = '';
    if( !empty($data) ){
        foreach ($data as $url) {
            $fileContent .= "BAD_IMGS[]=\"".$url."\"\n";
        }
    }

    if( empty($fileContent) || $fileContent == '' ){
        $fileContent = "BAD_IMGS[]=\"\"";
    }

    $continue = false;
    if( writeFile('bad_imgs.ini', $fileContent) ){
        $continue = true;
    }

    if ($continue){
        echo "All Fine!";
    }else{
        echo "Something wrong!";
    }
}