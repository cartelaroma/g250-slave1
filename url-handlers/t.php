<?php

/**
 *
 * Tracker Image URL Handler - It handles requests for (Campaign) Email Tracking
 *
 * @author Majid Hameed
 * @since Novemeber 25, 2012
 *
 */

use Guzzle\Http\Client;

$scriptName = basename(__FILE__, '.php');

$logger->debug($scriptName . ' - Valid Request - Request Data - ' . json_encode($requestData));

// POST data to be saved in database
$client = new Client($t_processor);

try {

    $requestData['requestHeaders']['SCRIPT_NAME']  =  "/".$scriptName . '.php';

	$request = $client->post('', null, $requestData, $requestOptions);

	// Send the request and parse the JSON response into an array
	$request->send()->json();

	//Display Image
	$fileName='eh.gif';
	$filePath="img/$fileName";
	$size=filesize($filePath);
	$contentType='image/gif';

	$logger->info($scriptName . ' - response - ' . $filePath);

	header("HTTP/1.1 200 OK");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("cache-Control: no-store, no-cache, must-revalidate");
	header("cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header("Content-Type: $contentType");
	header("Content-Length: $size");
	readFile($filePath);
	exit;
} catch (Exception $e) {
	$logger->error($e->getMessage());
}