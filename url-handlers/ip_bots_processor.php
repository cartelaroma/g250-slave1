<?php
/**
 * Created by PhpStorm.
 * User: yancy
 * Date: 21/10/15
 * Time: 5:39
 */
require_once 'vendor/autoload.php';
require 'constants.php';
require 'common.php';

ini_set('display_errors', 'On');

$scriptName = basename(__FILE__, '.php');

try{
    saveIps($ipBotsUrl);

    //loadIps();
}catch (Exception $e){
    $logger->info($scriptName . ' - Something wrong when processing - ' . json_encode($e));
}

function saveIps($url){
    $ch = curl_init($url);
    $fp = fopen('/usr/local/g250/url-handlers/bad_ips.txt', 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
}

function loadIps(){
    var_dump(loadBadIPListJson('bad-ips2.txt'));
}

