<?php
/**
 * Created by PhpStorm.
 * User: yancy
 * Date: 16/06/14
 * Time: 18:46
 */

function get_post_string($params, $mask){
    foreach ($params as $key => &$val) {
        if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_params[] = 'mask=' . urlencode($mask);
    return $post_string = implode('&', $post_params);
}

function curl_post_async($processor, $params, $mask)
{

    $post_string = get_post_string($params, $mask);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $processor);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, 'curl');
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    $result = curl_exec($ch);
    curl_close($ch);
}

function curl_socket_post_async($url, $params)
{
    foreach ($params as $key => &$val) {
        if (is_array($val)) $val = implode(',', $val);
        $post_params[] = $key.'='.urlencode($val);
    }
    $post_string = implode('&', $post_params);

    $parts=parse_url($url);

    $fp = fsockopen($parts['host'],
        isset($parts['port'])?$parts['port']:80,
        $errno, $errstr, 30);

    pete_assert(($fp!=0), "Couldn't open a socket to ".$url." (".$errstr.")");

    $out = "POST ".$parts['path']." HTTP/1.1\r\n";
    $out.= "Host: ".$parts['host']."\r\n";
    $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
    $out.= "Content-Length: ".strlen($post_string)."\r\n";
    $out.= "Connection: Close\r\n\r\n";
    if (isset($post_string)) $out.= $post_string;

    fwrite($fp, $out);
    fclose($fp);
}


function getURL_old_to_delete($requestData, $client){
    $mask = $requestData['mask'];
    if ($db = new SQLite3('db/cached.db')) {
        $db->exec('CREATE TABLE IF NOT EXISTS email_content_url (ID int, Url varchar(500), PRIMARY KEY (ID))');
        $queryFormat = "SELECT url FROM email_content_url WHERE ID='%s'";
        $insertFormat = "INSERT INTO email_content_url(ID, Url) VALUES (%s, '%s')";

        preg_match_all('/[\\da-f]/', $mask, $matches);

        $hex_string = implode('', $matches[0]);
        $number = hexdec($hex_string);
        $urlID = $number - 17;

        $query = sprintf($queryFormat, $urlID);

        $q = @$db->querySingle($query);

        if ($q === false || $q == null){
            //GET FROM MASTER SERVER
            try{
                $request = $client->post('', null, $requestData, array('timeout' => 59, 'connect_timeout' => 59));
                $url = $request->send()->json();
                $insertQuery = sprintf($insertFormat, $urlID, $url);
                $db->exec($insertQuery);

            } catch (Exception $e) {
                $logger->error($e->getMessage());
            }
        }else{
            $url = $q;
        }
    }
    return $url;

}

function getURL($requestData, $client, $img = false){
    $mask = $requestData['mask'];
    if ($db = new SQLite3('db/cached.db')) {
        $db->exec('CREATE TABLE IF NOT EXISTS email_content_url (ID int, Url varchar(500), localURL varchar(500), PRIMARY KEY (ID))');
        $queryFormat = (!$img) ? "SELECT url FROM email_content_url WHERE ID='%s'" : "SELECT ID, Url, localURL FROM email_content_url WHERE ID='%s'";
        $insertFormat = (!$img) ? "INSERT INTO email_content_url(ID, Url) VALUES (%s, '%s')" : "INSERT INTO email_content_url(ID, Url, localURL) VALUES (%s, '%s', '%s')";

        preg_match_all('/[\\da-f]+/', $mask, $matches);

        $jobid = hexdec($matches[0][count($matches[0])-1]);

        $requestData['jobID'] = $jobid;

        unset($matches[0][count($matches[0])-1]);

        $hex_string = implode('', $matches[0]);
        $number = hexdec($hex_string);
        $urlID = $number - 17;

        //return array("urlID" => $urlID, "jobID" => $jobid, "mask" => $mask);

        $query = sprintf($queryFormat, $urlID);

        if ($img){
            $q = @$db->querySingle($query, true);
        }else{
            $q = @$db->querySingle($query);
        }

        if ($q === false || $q == null){
            //GET FROM MASTER SERVER
            try{
                $request = $client->post('', null, $requestData, array('timeout' => 59, 'connect_timeout' => 59));
                $url = $request->send()->json();

                if ($img){
                    $randomName = "img" . $urlID;
                    $count = 0;
                    do{
                        if ($count++ == 2){
                            $localURL = 'img/eh.gif';
                            break;
                        }
                    }while (!($localURL = saveImage($url, $randomName)));
                    $insertQuery = sprintf($insertFormat, $urlID, $url, $localURL);
                    $url = array("Url" => $url, "localURL" => $localURL);
                }else{
                    $insertQuery = sprintf($insertFormat, $urlID, $url);
                }
                $db->exec($insertQuery);

            } catch (Exception $e) {
                $logger->error($e->getMessage());
            }
        }else{
            if ($img){
                $url = $q; //Return only $q['localURL']
            }else{
                $url = $q;
            }
        }
    }
    return $url;

}

function decrypt_number( $code ){
    preg_match_all('/[\\da-f]/', $code, $matches);

    $hex_string = implode('', $matches[0]);
    $number = hexdec($hex_string);
    $value = $number - 17;
    return $value;
}

function decrypt_script( $code ){
    preg_match_all('/[\\da-f]/', $code, $matches);

    $hex_string = implode('', $matches[0]);
    $number = hexdec($hex_string);
    $value = $number - 17;
    return chr($value);
    //return $value;

    //String integerString = Integer.toHexString((int) ((char) footPrint.charAt(0)) + PAD_NUMBER).toLowerCase();
}

function decrypt ( $url ){

    $url_array = explode("/", $url);
   // echo "url: " . json_encode( $url_array ) . "<br>";

    $i = -1;
    foreach ($url_array as $code){
        $i++;
        if (strlen($code) < 7){
            continue;
        }

        $clue = decrypt_number(substr($code, 0, 3));
        $clue %= 30;
        $cad = substr($code, 0, 3);
        //echo "characters: $cad, clue: $clue i: $i <br>";
        if ($clue == $i){
            //echo "pre fijo: " . substr($code, 2, 4) . "<br>";
            $pref = decrypt_script(substr($code, 3, 4));
            //echo "suf fijo: " . substr($code, count($code) - 3) . "<br>";
            $suf = decrypt_number(substr($code, count($code) - 4));
            $suf %= 30;
            $mask = substr($code, 7, ($suf-7));
            $hashID =  $suf;
            $cs = substr($code, $suf, count($code) - 4);
            $urlID = decrypt_number($mask);

            //echo "clue: $clue - pref: $pref - suf: $suf - mask: $mask - cs: $cs - urlID: $urlID";
            return array("clue" => $clue, "pref" => $pref, "urlID" => $urlID, "mask" => $mask, "hashID" => $hashID, "cs" => $cs);
        }
    }
    return false;
}

function decodeEmail( $cad ){
    $MASK_PADDING_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.~+";
	$CODED_MASK_PADDING      = "89FGH@IJKLabcdeABCD+EtuvwxyzM-NOPmnopqrsQRS0fghijkl1.23TUVW4_567XYZ";
    $res = "";

    $cad_array = str_split($cad);

    foreach ($cad_array as $c){
        $res .= substr($CODED_MASK_PADDING, strpos($MASK_PADDING_CHARACTERS, $c), 1);
    }
	return $res;
}

function base64url_encode($data) {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data) {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

function saveImage1($url, $name){
    $ch = curl_init($url);
    $fp = fopen('/usr/local/g250/url-handlers/img'.$name, 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
}

function saveImage($url, $name){
    $url = $url;
    $img = 'img_cache/' . $name;
    $eUrl = getLastEffectiveUrl($url);
    if (exif_imagetype($eUrl)){
        file_put_contents($img, file_get_contents(getLastEffectiveUrl($eUrl)));
    }else{
        return FALSE;
    }
    return $img;
}

function getLastEffectiveUrl($url)
{
    // initialize cURL
    $curl = curl_init($url);
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_FOLLOWLOCATION  => true,
    ));

    // execute the request
    $result = curl_exec($curl);

    // fail if the request was not successful
    if ($result === false) {
        curl_close($curl);
        return null;
    }

    // extract the target url
    $redirectUrl = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
    curl_close($curl);

    return $redirectUrl;
}

?>