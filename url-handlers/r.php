<?php
/**
 * Created by PhpStorm.
 * User: yancy
 * Date: 15/05/14
 * Time: 8:41
 */

use Guzzle\Http\Client;

$scriptName = basename(__FILE__, '.php');

$logger->debug($scriptName . ' - Valid Request - Request Data - ' . json_encode($requestData));

// POST data to be saved in database
$client = new Client($ga_processor);

try {
    $originalURL = getURL($requestData, $client);

    if ($originalURL==null || $originalURL=='INVALID-URL') {
        $originalURL = '/';
        $logger->error("$scriptName - Error INVALID-URL as response. Redirecting to $originalURL");
    }

    $_SERVER["SCRIPT_NAME"] =  "/".$scriptName . '.php';

    curl_post_async($r_processor, $_SERVER, $mask);

} catch (Exception $e) {
    $logger->error($e->getMessage());
}
?>
<?php if (!empty($originalURL)): ?>
    <?php if (JS_REDIRECT_ENABLED): ?>
        <!DOCTYPE html>
        <html>
        <head>
            <meta charset="utf-8" />
            <script type="text/javascript">
                setTimeout(redirect, 500);
                redirect();

                function redirect(){
                    window.location = "<?php echo $originalURL; ?>";
                }
            </script>
            <title></title>
        </head>
        </html>
    <?php else: ?>
        <?php header("") ?>
        <?php header("Location: $originalURL"); ?>
    <?php endif; ?>
<?php endif; ?>