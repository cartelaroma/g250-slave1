<?php
/**
 * Created by PhpStorm.
 * User: yancy
 * Date: 5/09/14
 * Time: 8:51
 */

require_once 'vendor/autoload.php';
require 'constants.php';
require 'common.php';
require 'utils.php';
use Guzzle\Http\Client;

ini_set('display_errors', 'Off');

$scriptName = basename(__FILE__, '.php');

//echo json_encode( decrypt($code) );

require_once 'filter_bots.php';

try{
// Process Request
if (empty($_REQUEST['c1']) || (BOT_FILTERING_ENABLED && isBadUserAgent($_SERVER['HTTP_USER_AGENT'], $BAD_UA_LIST))) {
    $logger->info($scriptName . ' - Invalid Request - Request Detail - ' . json_encode($_SERVER));
    return;
}
$code = $_REQUEST['c1'] ."/". $_REQUEST['c2'];

$decrypt_array = decrypt( $code );

$decodeEmail = decodeEmail($decrypt_array['cs']);

if (!filter_var($decodeEmail, FILTER_VALIDATE_EMAIL)) {
    $logger->info($scriptName . ' - invalid email ' . $decodeEmail);
    return;
}

if ($decrypt_array != false){
    $script = $decrypt_array["pref"];
    $mask = $decrypt_array["mask"];
}

//Check for possible bots
if (isset($_SERVER['REMOTE_ADDR']) && isPossibleBot($_SERVER['REMOTE_ADDR'])){
    $logger->info($scriptName . ' - Possible bot IP - Request Detail - ' . json_encode($_SERVER));
    $script = "x";
}

//Check for possible bots cloudflare
if (isset($_SERVER['HTTP_CF_CONNECTING_IP']) && isPossibleBot($_SERVER['HTTP_CF_CONNECTING_IP'])){
    $logger->info($scriptName . ' - Possible bot IP [CloudFlare] - Request Detail - ' . json_encode($_SERVER));
    $script = "x";
    #return;
}

//$mask = $_REQUEST['h'];

$_SERVER['QUERY_STRING'].="&cs=".$decrypt_array['cs'];

$requestData = array(
    'mask' => $mask,
    'requestHeaders' => $_SERVER
);

$logger->debug($scriptName . ' - Valid Request - Request Data - ' . json_encode($requestData));

// POST data to be saved in database
//$client = new Client($g_processor);
}catch (Exception $e) {
    $logger->error($e->getMessage());
    display_500();
}

try {
    $originalURL = "/" . $script . "/" . $mask . "/" . $decrypt_array['cs'];

    if ( $script == "u" ){
        require_once 'u.php';
    }
    if ( $script == "t" ){
        require_once 't.php';
    }
    if ( $script == "i" ){
        require_once 'i.php';
    }

    if ( $script == "x" ){
        require_once 'unsubscribe.php';
    }

    if ( $script == "r" ){
        require_once 'r.php';
    }

    if ( $script == "o" ){
        require_once 'o.php';
    }

    if ( $script == "h" ){
        require_once 'h.php';
    }

    //echo $originalURL;
    //return;

} catch (Exception $e) {
    $logger->error($e->getMessage());
}

?>