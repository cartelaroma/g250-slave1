#!/usr/bin/python

import os

active = []
configured = []
flag = 1

# Get active interfaces
active_interfaces = os.popen("ip add sh").readlines()

for item in active_interfaces:
    tmp = item.split(":")
    if len(tmp) > 1:
        if tmp[1].find("tun") > -1 and tmp[2].find("state UNKNOWN") > -1:
            active.append(tmp[1].strip())
    else:
        pass
#print active

# Get interfaces added by g250
interfaces= os.popen("ls /usr/local/g250/tunnel/tun*.startup | LANG=C sed 's/ //'").readlines()

for item in interfaces:
    configured.append(item.split("/usr/local/g250/tunnel/")[1].split(".")[0])
#print configured

# Check if all g250 interfaces are active
for item in configured:
    if item in active:
        pass
    else:
        # os.system("chmod +x /usr/local/g250/tunnel/%s.config" %item)
        os.system("chmod +x /usr/local/g250/tunnel/%s.startup" %item)
        # os.system("sh /usr/local/g250/tunnel/%s.config" %item)
        os.system("sh /usr/local/g250/tunnel/%s.startup" %item)

# check the interfaces
#for item in configured:
#    if item in active:
#        pass
#    else:
#        flag = 0

#if flag == 0:
#    print "Tunnel start script FAILED"
