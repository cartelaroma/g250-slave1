#!/usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------------------------------------------------------
# Name:        tunnel.py
# Purpose:     Make tunnel configuration
# Author:      Rolando Caner
# Created:     2014/03/08
# --------------------------------------------------------------------------------------

import os
import sys
from tunnel_utils import log
from tunnel_utils import check_address
from tunnel_utils import check_directory
from tunnel_utils import parse_arguments
from tunnel_utils import force_remove_slave_ovz as force_remove_slave
from tunnel_utils import check_tunnel_ovz as check_tunnel

USAGE = 'Usage: python tunnel2.py {start|stop|restart} {servant} ' \
        '{tunnelID1,master_public_address1,slave_public_address1,master_private_address1,slave_private_address1} ' \
        '{tunnelID2,master_public_address2,slave_public_address2,master_private_address2,slave_private_address2} ' \
        '{tunnelID3,master_public_address3,slave_public_address3,master_private_address3,slave_private_address3} '

DEBUG = 0
LOGFILE = '/usr/local/g250/logs/tunnel.log'
FILE = "/etc/vtund.conf"

# --------------------------------------------------------------------------------------

# Get logging facility
logger = log(LOGFILE, DEBUG)

tmp = "ovz - "
for item in sys.argv:
    tmp = tmp + " " + item
logger.info(tmp)

try:
    command = sys.argv[1]
    server_type = sys.argv[2]

    if command != 'start' and command != 'stop' and command != 'restart':
        logger.error('ovz - command must be "start", "stop" or "restart"')
        raise NameError('command must be "start", "stop" or "restart"')

    if server_type != 'master' and server_type != 'servant':
        logger.error('ovz - server_type must be master or servant')
        raise NameError('server_type must be master or servant')

    config, tunnelIDs = parse_arguments(sys.argv)

except IndexError:
    logger.error('ovz - %s' % USAGE)
    raise IndexError(USAGE)

# --------------------------------------------------------------------------------------
# Slave configuration
# --------------------------------------------------------------------------------------

if server_type == 'servant':

    # Make temporary directory
    check_directory()
    session = ""

    if command == 'start' or command == 'restart':

        # Check addresses
        for key in tunnelIDs:
            for item in (config[key]['master_public_address'],
                         config[key]['slave_public_address'],
                         config[key]['master_private_address'],
                         config[key]['slave_private_address']):
                pass
                #check_address(item)

        # Clear last tunnel connection
        force_remove_slave()

        # Name server
        os.system('echo "nameserver 8.8.8.8" > /etc/resolv.conf; echo "nameserver 8.8.4.4" >> /etc/resolv.conf; cat  /etc/resolv.conf')

        # Default rules
        os.system('echo 1 > /proc/sys/net/ipv4/ip_forward')
        os.system('sysctl -w net.netfilter.nf_conntrack_max=200000')
        os.system('iptables --flush')
        os.system('ip6tables --flush')                        # testing

        # Default rules IPv6
        # todo: capture the default route
        # os.system('sysctl net.ipv6.conf.all.forwarding=1')  # testing
        # os.system('sysctl net.ipv6.conf.all.proxy_ndp=1')   # testing
        # os.system('sysctl net.ipv6.conf.all.autoconf=0')    # testing
        # os.system('sysctl net.ipv6.conf.all.accept_ra=0')   # testing
        # todo: apply the default route

        # Update iptables rules
        net_dicc_tun = dict()    # testing - tunnel networks
        net_dicc_priv = dict()   # testing - master private addresses

        count = 0
        ipv6_flag = False
        for key in tunnelIDs:
            count += 1

            if config[key]['master_private_address'].find("::") == 0:
                ipv6_flag = True
                cmd = 'ip6tables -t nat -A POSTROUTING -s %s -j SNAT --to %s' \
                      % (config[key]['master_private_address'], config[key]['slave_public_address'])
                print cmd
                os.system(cmd)
            else:
                cmd = 'iptables -t nat -A POSTROUTING -s %s -j SNAT --to %s' \
                      % (config[key]['master_private_address'], config[key]['slave_public_address'])
                print cmd
                os.system(cmd)

            # creates the configuration for incoming connection
            if config[key]['master_private_address'].find("10.250") == 0:
                cmd = 'iptables -t nat -A PREROUTING -d %s -p tcp --dport 25 -j DNAT --to %s' \
                      % (config[key]['slave_public_address'], config[key]['master_private_address'])
                print cmd
                os.system(cmd)

            elif config[key]['master_private_address'].find("::10.250")== 0:
                cmd = 'ip6tables -t nat -A PREROUTING -d %s -p tcp --dport 25 -j DNAT --to %s' \
                      % (config[key]['slave_public_address'], config[key]['master_private_address'])
                print cmd
                os.system(cmd)

            # testing unique tunnel feature
            try:
                t_net = config[key]['master_private_address'].split(".")
                net = t_net[0] + "." + t_net[1] + ".0.0"                        # 10.x.0.0  - create a tunnel for each server
                if net not in net_dicc_tun.keys():
                    net_dicc_tun[net] = key                                     # tunnelID
            except IndexError:
                pass

            # end testing

        # IPv4 tunnel
        if not ipv6_flag:
            print net_dicc_tun  # {'10.250.0.0': 'tun340_1', '10.1.0.0': 'tun340_329'}

            # Make tunnels
            for net_addr in net_dicc_tun.keys():
                key = net_dicc_tun[net_addr]
                
                session += """
# Session '%s'.
%s {
  passwd  Ro&^BW;       # Password
  device %s;            # Device tunX
  type  tun;            # IP tunnel
  proto udp;            # UDP protocol
  compress  no;         # LZO compression level 9
  encrypt  no;          # Encryption
  keepalive yes;        # Keep connection alive
  up {
        # Connection is Up
        ifconfig "%%%% %s pointopoint %s mtu 1450";
        ip "route add %s/16 via %s dev %s";     # testing
  };
}
""" % (key, key, key, config[key]['slave_private_address'], config[key]['master_private_address'], net_addr, config[key]['master_private_address'], key)

            _config = """
options {
  port 8080;            # Listen on this port.

  # Syslog facility
  syslog        daemon;

  # Path to various programs
  ppp           /usr/sbin/pppd;
  ifconfig      /sbin/ifconfig;
  route         /sbin/route;
  firewall      /sbin/ipchains;
  ip            /sbin/ip;
}

# Default session options
default {
  compress no;          # Compression is off by default
  speed 0;              # By default maximum speed, NO shaping
}

%s
""" % session

            # Write file
            f = open(FILE, "w")
            f.write(_config)
            f.close()
            os.system('/usr/sbin/vtund -s')
            #

        # IPv6 tunnel
        else:
            print net_dicc_tun   # {'::10.250.0.0': 'tun341_1', '10.250.0.0': 'tun340_1', '10.1.0.0': 'tun340_329', '::10.1.0.0': 'tun341_329'}

            # Make tunnels
            cmd = 'modprobe ip_gre'
            print cmd
            os.system(cmd)

            for net_addr in net_dicc_tun.keys():     # testing
                if not net_addr.startswith("::"):    # exclude :: networks
                    key = net_dicc_tun[net_addr]

                    # todo: remove old tunnel connection of the same peer
                    cmd = 'ip tun del %s' % key
                    print cmd
                    os.system(cmd)

                    cmd = 'ip tu ad %s mode gre local %s remote %s ttl 64' \
                          % (key, config[key]['slave_public_address'], config[key]['master_public_address'])
                    print cmd
                    os.system(cmd)

                    cmd = 'ip ad ad dev %s ::%s peer ::%s/64' \
                          % (key, config[key]['slave_private_address'], config[key]['master_private_address'])
                    print cmd
                    os.system(cmd)

                    cmd = 'ip li se dev %s up' % key
                    print cmd
                    os.system(cmd)

                    cmd = "ip route del ::%s/16" % net_addr
                    print cmd
                    os.system(cmd)

                    cmd = "ip route add ::%s/16 via ::%s dev %s " \
                          % (net_addr, config[key]['master_private_address'], key)
                    print cmd
                    os.system(cmd)

        # Check tunnel status
        if not ipv6_flag:
            if check_tunnel():
                for key in tunnelIDs:
                    logger.info('ovz - success to start tunnel %s' % key)
                    os.system('echo up > /usr/local/g250/tunnel/%s.status' % key)
            else:
                for key in tunnelIDs:
                    logger.info('ovz - failed to start tunnel %s' % key)
                    os.system('echo down > /usr/local/g250/tunnel/%s.status' % key)
        else:
            # todo fix this
            for key in tunnelIDs:
                logger.info('kvm - success to start tunnel %s' % key)
                os.system('echo up > /usr/local/g250/tunnel/%s.status' % key)

    elif command == 'stop':
        pass

        # # Clear last tunnel connection
        # force_remove_slave()
        #
        # logger.info('ovz - success to stop tunnels')
        # # os.system('echo down > /usr/local/g250/tunnel/%s.status' % tunnel_iface)
