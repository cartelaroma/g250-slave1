#!/usr/bin/python
# -*- coding: utf-8 -*-
# --------------------------------------------------------------------------------------
# Name:        tunnel.py
# Purpose:     Make tunnel configuration
# Author:      Rolando Caner
# Created:     2013/06/03
# --------------------------------------------------------------------------------------

import os
import sys
from tunnel_utils import log
from tunnel_utils import check_address
from tunnel_utils import check_directory
from tunnel_utils import del_startup_files
from tunnel_utils import parse_arguments
from tunnel_utils import force_remove_slave_kvm as force_remove_slave
from tunnel_utils import check_tunnel_kvm as check_tunnel

USAGE = 'Usage: python tunnel.py {start|stop|restart} {servant} ' \
        '{tunnelID1:slave_public_address1:master_private_address1:slave_private_address1} ' \
        '{tunnelID2:slave_public_address2:master_private_address2:slave_private_address2} ' \
        '{tunnelID3:slave_public_address3:master_private_address3:slave_private_address3} ' \
        '{master_public_address}'

DEBUG = 0
LOGFILE = '/usr/local/g250/logs/tunnel.log'


# --------------------------------------------------------------------------------------

# Get logging facility
logger = log(LOGFILE, DEBUG)

tmp = "kvm - "
for item in sys.argv:
    tmp = tmp + " " + item
logger.info(tmp)

try:
    command = sys.argv[1]
    server_type = sys.argv[2]

    if command != 'start' and command != 'stop' and command != 'restart':
        logger.error('kvm - command must be "start", "stop" or "restart"')
        raise NameError('command must be "start", "stop" or "restart"')

    if server_type != 'master' and server_type != 'servant':
        logger.error('kvm - server_type must be master or servant')
        raise NameError('server_type must be master or servant')

    config, tunnelIDs = parse_arguments(sys.argv)

except IndexError:
    logger.error('kvm - %s' % USAGE)
    raise IndexError(USAGE)

# --------------------------------------------------------------------------------------
# Slave configuration 
# --------------------------------------------------------------------------------------

if server_type == 'servant':

    # Make temporary directory
    check_directory()

    if command == 'start' or command == 'restart':

        # Check addresses
        for key in tunnelIDs:
            for item in (config[key]['master_public_address'],
                         config[key]['slave_public_address'],
                         config[key]['master_private_address'],
                         config[key]['slave_private_address']):
                check_address(item)

        # Make tunnels
        os.system('modprobe ip_gre')
        os.system('echo 1 > /proc/sys/net/ipv4/ip_forward')
        os.system('iptables --flush')

        for key in tunnelIDs:

            # Clear last tunnel connection
            force_remove_slave(key, config)

            os.system('iptunnel add %s mode gre local %s remote %s ttl 255'
                      % (key, config[key]['slave_public_address'], config[key]['master_public_address']))

            os.system('ifconfig %s %s/30' % (key, config[key]['slave_private_address']))

            os.system('ifconfig %s up' % key)

            os.system('ifconfig %s pointopoint %s' % (key, config[key]['master_private_address']))

            os.system('ifconfig %s multicast' % key)

            os.system('iptables -t nat -A POSTROUTING -s %s -j SNAT --to %s'
                      % (config[key]['master_private_address'], config[key]['slave_public_address']))

            os.system('iptables -t nat -A PREROUTING -d %s ! -s %s -p tcp --dport 25 -j DNAT --to %s'
                      % (config[key]['slave_public_address'], config[key]['master_public_address'],
                         config[key]['master_private_address']))

            # Check tunnel status
            if check_tunnel(key):
                logger.info('kvm - success to start tunnel %s' % key)
                os.system('echo up > /usr/local/g250/tunnel/%s.status' % key)

            else:
                logger.error('kvm - failed to start tunnel %s' % key)
                os.system('echo down > /usr/local/g250/tunnel/%s.status' % key)
        
    elif command == 'stop':

        # Clear last tunnel connection
        for _key in tunnelIDs:
            force_remove_slave(_key, config)
       
        logger.info('kvm - success to stop tunnel')
        # os.system('echo down > /usr/local/g250/tunnel/%s.status' % tunnel_iface)