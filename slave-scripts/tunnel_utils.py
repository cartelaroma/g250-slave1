import os
import logging

def log(logfile, debug):
    """
    Logging facility ERROR, INFO, DEBUG
    """
    logger = logging.getLogger('Log')
    hdlr = logging.FileHandler(logfile)
    formatter = logging.Formatter('%(asctime)s\t %(levelname)s\t%(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    if debug == 1:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    return logger

def check_directory():
    if not os.path.exists('/usr/local/g250/tunnel/'):
        os.makedirs('/usr/local/g250/tunnel/')

def check_address(addr):
    value = addr.split('.')
    if value.__len__() != 4:
        # log.error('%s is not a valid address' % addr)
        raise NameError('%s is not a valid address' % addr)
    try:
        if int(value[0]) > 255 or int(value[1]) > 255 or int(value[2]) > 255 or int(value[3]) > 255:
            # log.error('%s is not a valid address' % addr)
            raise NameError('%s is not a valid address' % addr)
    except ValueError:
        # log.error('%s is not a valid address' % addr)
        raise NameError('%s is not a valid address' % addr)

def del_startup_files(_key):
    if os.path.exists('/usr/local/g250/tunnel/%s.startup' % _key):
        os.system('rm /usr/local/g250/tunnel/%s.startup' % _key)

def check_command(_command, value):
    p = os.popen(_command)
    for line in p.readlines():
        if line.find(value) > -1:
            return 1
    return 0

def check_tunnel_kvm(_key):
    # Check if the tunnel is up/down
    if check_command('ifconfig', _key):
        return 1
    else:
        return 0

def check_tunnel_ovz():
    # Check if the tunnel is up/down
    if check_command('ps ax | grep vtun', "waiting for connections on port"):
        return 1
    else:
        return 0

def force_remove_slave_ovz():
    os.system("iptables -t nat --flush")
    os.system("ip6tables -t nat --flush")

    # this delete all tunnels
    tun_ifaces = os.popen("ps fax | grep 'vtun' ").readlines()
    for iface in tun_ifaces:
        if iface.find("grep") > -1:
            pass
        else:
            pid = iface.split()[0]
            os.system("kill -9 %s" % pid)

def force_remove_slave_kvm(_key, config):
        os.system("ifconfig %s down" % _key)
        os.system("iptunnel del %s" % _key)

        # os.system("iptables -t nat --flush")
        os.system("iptables -t nat -D POSTROUTING -s %s -j SNAT --to %s"
                  % (config[_key]['master_private_address'],
                     config[_key]['slave_public_address']
                     )
                  )
        os.system("iptables -t nat -D PREROUTING -d %s ! -s %s -p tcp --dport 25 -j DNAT --to %s"
                  % (config[_key]['slave_public_address'],
                     config[_key]['master_public_address'],
                     config[_key]['master_private_address']
                     )
                  )

def parse_arguments(data):

    config = {}
    tunnelIDs = []

    for data in data[3:]:
        _tmp = data.split(",")

        tunnelID = _tmp[0]
        master_public_address = _tmp[1]
        slave_public_address = _tmp[2]
        master_private_address = _tmp[3]
        slave_private_address = _tmp[4]
        tunnel_iface = 'tun' + tunnelID
        tunnelIDs.append(tunnel_iface)

        config[tunnel_iface] = {}
        config[tunnel_iface]['tunnelID'] = tunnelID
        config[tunnel_iface]['master_public_address'] = master_public_address
        config[tunnel_iface]['slave_public_address'] = slave_public_address
        config[tunnel_iface]['master_private_address'] = master_private_address
        config[tunnel_iface]['slave_private_address'] = slave_private_address

    return config, tunnelIDs
